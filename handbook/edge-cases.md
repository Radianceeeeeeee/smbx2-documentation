# Edge Cases and Keeping Compatibility

This build of SMBX2 may be stable, but the engine is
being actively developed, so current problems will be addressed for
future releases. As such, certain behaviour is subject to change in
later versions, and while these are unlikely to cause problems for
everyday level design, when delving into complex interactions they may
present issues. Below are the most important points that you need to
avoid when designing levels.

## Interactions Between Certain New NPCs

We’re still missing some new technology to streamline
interactions between NPCs and, as such, various interactions between new
NPCs and other new NPCs can be buggy or cause other unexpected
behaviour. These behaviours are subject to change as soon as we have the
new technology required to do so. Please refrain from exploiting
them.
