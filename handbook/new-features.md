# New Features

## The \_templates Folder

A lot of features in this section make use of specific
files and need those to be formatted in a certain way in order to
function. We have included a folder called “\_templates”, which includes
various dummy files for different purposes. Here is how you can use
them:

### ach-n.ini

Template for an achievement definition. When put into
an episode’s “achievements” folder, this will show up as one of the
achievements on the launcher. More info on achievements can be found
further down in the document.

### Dummy.png

A generic image file referred to in multiple other
configuration files in the dummy folder. Only needs to be copied for
testing before the image is replaced.

### background2-n.ini, background2-n.txt

A template for parallaxing backgrounds, discussed in
the section below. Copy it and Dummy.png to your level folder and rename
the “n” in the file to the number of the background you want to replace,
then make changes to the file until you get the desired result\! Your
should, however, delete all the fields you aren’t using, since some will
override others and give unexpected results\!

### background-n.ini, background-n.txt

A template for new BGOs in the 751-1000 range detailed
below. Take a look in the files after copying them over to your level
folder to see different customization options. The “ini” file determines
the editor appearance, while the “txt” file has effects on the game
while it’s played. Rename the “n” in the files to the ID you wish to
occupy and make sure the BGO has an image file associated with it (for
example background-751.png).

### Block-n.lua, block-n.ini

A template for new Blocks in the 751-1000 range
detailed below. Rename the “n” in the file to the number of the Block ID
you want to occupy and make sure the Block has a corresponding image
file associated with it. The example block-n.lua will simply destroy the
block as a check to see if everything works. The “ini” file determines
the editor appearance, while the “lua” file has effects on the game
while it’s played.

### block-n.txt

A template for a Block config file for existing Blocks.
Includes defaults for all configurable options. Excess options can be
removed.

### npc-n.lua, npc-n.ini

A template for new NPCs in the 751-1000 range detailed
below. Rename the “n” in the file to the number of the NPC ID you want
to occupy and make sure the NPC has a corresponding image file
associated with it. The example npc-n.lua will simply jump up and down
in place, as a check to see if everything works. The “ini” file
determines the editor appearance, while the “lua” file has effects on
the game while it’s played.

### npc-n.txt

A template for a NPC config file for existing NPCs.
Includes defaults for all configurable options. Excess options can be
removed.

### example.lua

A template for a LunaLua library file. Contains various
example functions, but doesn’t do anything by itself. Copy the file over
and load it to get started more easily with creating your own
libraries.

### particles\_example.ini, ribbon\_example.ini

Configuration files for particle emitters and ribbon
trails for the particles library discussed further below in the
handbook. Copy the files over and play around with the configuration
options to get a feel for how the systems are set up\! Don’t forget to
load the emitter and draw it, too\! More details in the section about
particles.

### music.ini

Configuration files for the global music locations.
Allows you to customise music on a per-level or per-episode basis,
depending on whether the files are placed in the level or episode
folder. Simply rewrite the file paths as needed.

### standard.vert, standard.frag

Example files for GLSL shaders. Shaders and the details
of these files are discussed further in the Shader Programming section
of the handbook.

## Parallaxing Backgrounds

Creating parallaxing backgrounds in levels has been
streamlined to the point where no user-written lua code is necessary for
most purposes.

Parallaxing backgrounds are now automatically applied
based on the file name of a corresponding .txt (or .ini) file:

![](images/image26.png)

Worth noting is that the naming for the txt files follows SMBX’s
internal numbering for
backgrounds, which may sometimes differ from the numbering of the .png
files. If the numbers are off in certain places, however, they’re never
off by more than one. The example above replaces the SMW Forest
background. The correct numbering is visible in the editor when
selecting a section
background.![](images/image3.png)

The txt file can pull from other images in the folder
and define the features of the background’s individual layers, like
so:

```ini
[Backdrop]
name="BG"
depth=450
alignY=TOP
img="prlx_clouds.png"
fitY=true
repeatX=true
speedX=-0.1

[Grid1]
name="Grid1"
depth=150
alignY=BOTTOM
img="prlx_foreground.png"
repeatX=true
repeatY=true
```

-----

Below is a list of parameters for each layer of the
background:

<table>
  <thead><tr>
    <th>Field</th>
    <th></th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>img</td>
    <td>(file name)</td>
    <td>REQUIRED: the image to draw in this layer</td>
  </tr>
  <tr>
    <td>name</td>
    <td>(string)</td>
    <td>A name for the layer, used by Background:Get. Defaults to "Layer#", where # is the layer index</td>
  </tr>
  <tr>
    <td>x, y</td>
    <td>(numbers)</td>
    <td>layer offset from top left of boundary, defaults to 0,0</td>
  </tr>
  <tr>
    <td>depth</td>
    <td>(number or INFINITE)</td>
    <td>Depth at which to position the layer (0 = in line with scene,&nbsp;&nbsp;&amp;gt;0 = behind scene, &amp;lt;0 = in front of scene), computed from fit if not supplied Default of depth.INFINITE if fit is disabled</td>
  </tr>
  <tr>
    <td>fitX, fitY</td>
    <td>(booleans)</td>
    <td>Should the layer attempt to fit its parallaxing to the boundaries?</td>
  </tr>
  <tr>
    <td>priority</td>
    <td>(number)</td>
    <td>Render priority, computed from depth if not supplied</td>
  </tr>
  <tr>
    <td>opacity</td>
    <td>(number)</td>
    <td>How transparent this layer should be, defaults to 1</td>
  </tr>
  <tr>
    <td>speedX, speedY</td>
    <td>(numbers)</td>
    <td>How fast this layer should move of its own volition, defauts to 0,0</td>
  </tr>
  <tr>
    <td>parallaxX, parallaxY</td>
    <td>(numbers)</td>
    <td>Override for parallax scrolling speed (0 = no scrolling, 1 = scroll with scene,  &amp;gt;1 = scroll faster than scene)</td>
  </tr>
  <tr>
    <td>repeatX, repeatY</td>
    <td>(booleans or numbers)</td>
    <td>How many repeats of this image should be applied? 0 or true = infinite repeats, 1 = no repeats, &amp;gt;1 = n repeats</td>
  </tr>
  <tr>
    <td>padX, padY</td>
    <td>(numbers)</td>
    <td>Padding to place between repeated images</td>
  </tr>
  <tr>
    <td>marginLeft, marginRight</td>
    <td>(numbers)</td>
    <td>Padding to the side of the layer</td>
  </tr>
  <tr>
    <td>marginTop, marginBottom</td>
    <td>(numbers)</td>
    <td>Padding to the top/bottom of the layer</td>
  </tr>
  <tr>
    <td>margin</td>
    <td>(table of numbers)</td>
    <td>A table containing all 4 margins, named</td>
  </tr>
  <tr>
    <td>hidden</td>
    <td>(boolean)</td>
    <td>Should this layer be hidden? Defaults to false</td>
  </tr>
  <tr>
    <td>frames</td>
    <td>(integer)</td>
    <td>Number of animation frames, defaults to 1</td>
  </tr>
  <tr>
    <td>framespeed</td>
    <td>(integer)</td>
    <td>Frame timer between animation frames, defaults to 8</td>
  </tr>
  <tr>
    <td>alignX, alignY</td>
    <td>(LEFT, TOP, RIGHT, BOTTOM, CENTRE)</td>
    <td>Alignment for the x and y coordinates. Defaults to LEFT/TOP</td>
  </tr>
</tbody></table>

In addition to the per-layer properties, a “fill-color”
property can be defined, which can be set to color constants or
hexadecimal colours. Further information on the color constants can be
found below.

![](images/image3.png)

You should define as few of these per-layer properties
as you need, since some of them will override the effects of others
(such as parallaxX and parallaxY overriding depth).

-----

## TXT Files for Blocks

Blocks can now be modified through TXT files, similar
to how NPCs have been able to since SMBX 1.3. The following parameters
are supported:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>frames</td>
    <td>number of frames on the block sprite, defaults to 1</td>
  </tr>
  <tr>
    <td>framespeed</td>
    <td>Animation speed of the block. Lower=faster, defaults to 8</td>
  </tr>
  <tr>
    <td>width, height</td>
    <td>Determines the dimensions of a frame on the spritesheet. Automatically inferred from spritesheet dimensions and frame count by default.</td>
  </tr>
  <tr>
    <td>sizable</td>
    <td>If true, the block is a sizeable block.</td>
  </tr>
  <tr>
    <td>semisolid</td>
    <td>If true, the block is semisolid.</td>
  </tr>
  <tr>
    <td>passthrough</td>
    <td>If true, the block has no collision.</td>
  </tr>
  <tr>
    <td>lava</td>
    <td>If true, the block is lava.</td>
  </tr>
  <tr>
    <td>floorslope</td>
    <td>Defines a floor slope collision. The value defines the direction: -1, 1.</td>
  </tr>
  <tr>
    <td>ceilingslope</td>
    <td>Defines a ceiling slope collision. The value defines the direction: -1, 1.</td>
  </tr>
  <tr>
    <td>bumpable</td>
    <td>If true, the block is bumpable.</td>
  </tr>
  <tr>
    <td>smashable</td>
    <td>Enum for smashability. Values from 0 to 3 are valid.

1. Destroyed, but blocks the smashing entity.
2. Hit, but blocks the smashing entity.
3. Destroyed, but does not block the smashing entity.

    </td>
  </tr>
  <tr>
    <td colspan="2"><br />Darkness.lua-related:</td>
  </tr>
  <tr>
    <td>lightradius</td>
    <td>Radius of light</td>
  </tr>
  <tr>
    <td>lightbrightness</td>
    <td>Brightness of light</td>
  </tr>
  <tr>
    <td>lightoffsetx, lightoffsety</td>
    <td>Light offset relative to center of the sprite</td>
  </tr>
  <tr>
    <td>lightcolor</td>
    <td>Color constant or hex color specifying the light’s colour</td>
  </tr>
  <tr>
    <td>lightflicker</td>
    <td>If true, the light source flickers</td>
  </tr>
</tbody></table>

## TXT Files for Background Objects

Background objects can now be modified through TXT
files, similar to how NPCs have been able to since SMBX 1.3. The
following parameters are supported:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>frames</td>
    <td>number of frames on the bgo sprite, defaults to 1</td>
  </tr>
  <tr>
    <td>framespeed</td>
    <td>Animation speed of the bgo. Lower=faster, defaults to 8</td>
  </tr>
  <tr>
    <td>priority</td>
    <td>render priority, defaults to -85</td>
  </tr>
  <tr>
    <td>width, height</td>
    <td>Determines the dimensions of a frame on the spritesheet. Automatically inferred from spritesheet dimensions and frame count by default</td>
  </tr>
  <tr>
    <td>climbable</td>
    <td>Toggles climbability, defaults to false</td>
  </tr>
  <tr>
    <td colspan="2"><br />Darkness.lua-related:</td>
  </tr>
  <tr>
    <td>lightradius</td>
    <td>Radius of light</td>
  </tr>
  <tr>
    <td>lightbrightness</td>
    <td>Brightness of light</td>
  </tr>
  <tr>
    <td>lightoffsetx, lightoffsety</td>
    <td>Light offset relative to center of the sprite</td>
  </tr>
  <tr>
    <td>lightcolor</td>
    <td>Color constant or hex color specifying the light’s colour</td>
  </tr>
  <tr>
    <td>lightflicker</td>
    <td>If true, the light source flickers</td>
  </tr>
</tbody></table>

## TXT Files for Effects

The effects system currently used by new effects is exposed to the
user, and it allows customisation of effects per effect.txt-file
for IDs above 161.
Effect.txt files are seperated into layers of
spawned effects by a single spawner. The section below details all
configurable properties of individual effect layers:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>onInit</td>
    <td>defines the name of an onInit method defined in effectconfig.lua. Executes on spawn of this layer</td>
  </tr>
  <tr>
    <td>onTick</td>
    <td>defines the name of an onTick method defined in effectconfig.lua. Executes every frame of this layer</td>
  </tr>
  <tr>
    <td>onDeath</td>
    <td>defines the name of an onDeath method defined in effectconfig.lua. Executes when the effect layer dies.</td>
  </tr>
  <tr>
    <td>import</td>
    <td>Imports an effect template from effectconfig.lua</td>
  </tr>
  <tr>
    <td>template</td>
    <td>can be used by a layer to use another layer as a template, reducing duplication.</td>
  </tr>
  <tr>
    <td>img</td>
    <td>sprite used by this layer. Defaults based on name of the effect-n.txt file, accepts number (for internal effect image) or path to sprite image</td>
  </tr>
  <tr>
    <td>xOffset</td>
    <td>horizontal offset relative to anchor</td>
  </tr>
  <tr>
    <td>yOffset</td>
    <td>vertical offset relative to anchor</td>
  </tr>
  <tr>
    <td>gravity</td>
    <td>acceleration per frame, defaults to 0</td>
  </tr>
  <tr>
    <td>lifetime</td>
    <td>lifetime in frames, defaults to 65</td>
  </tr>
  <tr>
    <td>delay</td>
    <td>number of frames of delay before which this layer should be spawned, defaults to 0</td>
  </tr>
  <tr>
    <td>frames</td>
    <td>frame count of used image, defaults to 1</td>
  </tr>
  <tr>
    <td>framestyle</td>
    <td>framestyle, akin to how NPCs use it, defaults to 0</td>
  </tr>
  <tr>
    <td>framespeed</td>
    <td>framespeed, defaults to 8</td>
  </tr>
  <tr>
    <td>sound</td>
    <td>a sound to be played when this layer spawns. Can either be a number (for internal sfx) or a filepath to an audio file</td>
  </tr>
  <tr>
    <td>priority</td>
    <td>render priority, defaults to BACKGROUND but also accepts FOREGROUND</td>
  </tr>
  <tr>
    <td>xAlign, yAlign</td>
    <td>LEFT,TOP,MID,RIGHT,BOTTOM, defaults to MID, determines spawner align relative to xy coordinate passed to it</td>
  </tr>
  <tr>
    <td>spawnBindX, spawnBindY</td>
    <td>defaults to LEFT,TOP, determines spawned effect alignment relative to xy coordinate of spawner</td>
  </tr>
  <tr>
    <td>speedX, speedY</td>
    <td>initial velocity of effect</td>
  </tr>
  <tr>
    <td>maxSpeedX, maxSpeedY</td>
    <td>speed cap of this layer. Default: no speed cap (-1)</td>
  </tr>
  <tr>
    <td>opacity</td>
    <td>translucency, defaults to opaque (1)</td>
  </tr>
  <tr>
    <td>direction</td>
    <td>facing direction, defaults to -1 and only matters in conjunction with framestyle</td>
  </tr>
  <tr>
    <td>npcID</td>
    <td>multipurpose field emulating vanilla effect’s npcID field, defaults to 0</td>
  </tr>
  <tr>
    <td>angle</td>
    <td>initial angle of effect, defaults to 0</td>
  </tr>
  <tr>
    <td>rotation</td>
    <td>rotation speed of effect, defaults to 0</td>
  </tr>
  <tr>
    <td>variants</td>
    <td>number of variants this effect has. Variants multiply with frame count to result in total number of frames on the spritesheet</td>
  </tr>
  <tr>
    <td>variant</td>
    <td>determines which subset of frames (variant) to use. Defaults to 0 (0-indexed)</td>
  </tr>
</tbody></table>

Recreation of the vanilla brick block effect:

```ini
[first]
onTick=TICK_ARC
gravity=0.6
speedY=-6
speedX=-2
framespeed=3
frames=4
lifetime=500
maxSpeedY=10

[2]
template=first
speedX=2
img=1

[3]
template=first
speedY=-9
img=1

[4]
template=first
speedY=-10
speedX=2
sound=4
img=1
```

Recreation of the baby yoshi effect:

```ini
[1]
import=AI_BABYYOSHI
```

The AI\_BABYYOSHI is defined in
effectconfig.lua:

```lua
cfg.defaults.AI_BABYYOSHI = {
    framespeed = 10,
    onInit = "INIT_BABYYOSHI",
    onDeath = "DEATH_SPAWNNPCID",
    frames=2,
    variants=8
}
```

## NPC Codes

There are new additions to NPC codes in SMBX2 Beta 4.
While some are globally available, others define specific behaviour for
specific NPCs.

All available NPC codes can be viewed in the document
below. The buttons at the bottom of the linked document can be used to
navigate between different lists of NPC codes:

[SMBX2 NPC
Codes](https://docs.google.com/spreadsheets/d/1arkr_h1r1ZABZFpc22mcbi2N8QDw3hjNsb_zA0BCKJk/edit?usp%3Dsharing)

## The 751-1000 Range

We have added a reserved range of IDs for various
objects which are reserved entirely for people to customize on a
per-level or per-episode basis. The ranges are as follows:

  - Effects 751-1000
  - Blocks 751-1000
  - BGOs 751-1000
  - NPCs 751-1000

Once used, slots in these ranges will behave just like
any other item of that type.

For Blocks, BGOs and NPCs it’s important to remember
that their editor appearance of the object is defined by an ini file.
Examples of such ini files can be found in the appropriate subdirectory
of “SMBX\\data\\PGE\\configs\\SMBX2-Integration\\items”. You can copy
any of these as a template for your own custom object into your level or
episode folder in order to get started. Please refrain from editing the
example files directly, as doing so will mess up your
installation.

After the object is set up, press
F8 in the editor to
reload the level. Your new object can now be found through the Tileset
Itembox’s Tileset Editor.

![](images/image3.png)

Unused objects outside of those ranges are subject to
be used by us (the developers) in future releases, and should be left
as-is for the purposes of keeping your levels/episodes
compatible.

## \[block/npc\]-n.lua Files, Packs and Duplication

When taking a look in the “SMBX/data/scripts/npcs” and
“scripts/blocks” folders, you might notice that files are numbered
like their respective graphics files are. If you copy one of those files
(take the Thwimp, NPC 301, for example) into your level folder and
rename it to npc-751.lua, your local NPC 751 will act exactly like a
Thwimp would, without any additional lua code\! This modular system
makes it incredibly easy for people to duplicate basegame NPC and block
behaviour, but also makes drag-and-drop NPC and Block plugin packs very
easy to create, only requiring .lua, .png and .ini files for each object
that’s part of the pack\! Watch a tutorial on it here\!
[\[Part 1\]](https://youtu.be/LsT7ItnnVzY) [\[Part 2\]](https://youtu.be/o022ZArOaH0)

## Player Offsets

Previously, in SMBX 1.3, player sprites were always
anchored to the top left of a 100x100 pixel grid cell. This limitation
made it difficult to work with character sprites of different
dimensions, as player sprites would frequently dip into the ground from
the lack of vertical grid space.

![](images/image23.png)

SMBX2’s editor provides a tool that helps address this.
The tool is located in data/pge/pge\_calibrator.exe and opens up this
GUI:

![](images/image7.png)

First, load a spritesheet into the calibrator using the “open
sprite” button in the lower right. Once a player spritesheet is loaded,
a cell from the sheet is displayed on the right. As you can see here,
Luigi is already positioned much more centered on the grid than Mario
from earlier. The position on the grid is something you can help
visualise for yourself [using this
overlay](https://i.imgur.com/1dnW3g3.png).
Good practice is to make sure that where you want your hitbox to be is
in the same position for every cell. It saves a lot of work later
on\!

The general workflow with this program is as
follows:

1.  Select a frame on the character matrix. This will open an
    overlay where you can see all frames on the sheet. Make sure to
    check the checkboxes next to all sprites you want to use, and select
    one to begin
    working![](images/image13.png)
2.  Once you have your first sprite selected, hit the
    “Edit” button to enable hitbox modification for your entire
    spritesheet.
3.  Now it’s time to configure the global defaults for
    width, height and height while ducking. All units are measured in
    pixels. Focus for now on the character dimensions. It helps to jump
    ahead to “GFX Offsets of Frame” to try and roughly align the hitbox
    with the player, making sure the green rectangle covers the solid
    collision box you want. Hint: It’s often a good idea to make the
    hitbox slightly smaller than the sprite, to give players some
    leeway. Take notice of how Luigi’s arms and head are sticking out of
    the green rectangle a bit.  
    These dimensions are uniform across the entire spritesheet, so
    setting them up is a one-time manner.
4.  Next up are grab offsets. Unfortunately, it isn’t
    possible to switch from “top” to “side” type at the moment (it does
    nothing). You can also not change the offset for characters that use
    “top” offsets natively (Toad, Peach). You can change the offsets for
    characters based on Mario or Luigi, however. That’s how Wario’s
    Wario-Land-Style grab offset was made in SMBX2\!
5.  This lower section includes values unique to each
    frame. It is where having a uniform offset on the spritesheet really
    helps out. Because if your sprites are aligned well, you are able to
    use the same offset values for each frame. “Is duck frame” and
    “right direction” should also be checked where appropriate. Their
    names explain their function pretty well. The third checkbox doesn’t
    affect anything. Repeat this section for every sprite you use on the
    sheet.
6.  Now you can save the spritesheet with the button on
    the lower right\! This will save a .ini file named after the
    spritesheet you edited. If this file is in your level or episode
    directoy, SMBX2 will automatically load it for the character and
    powerup it’s named after\!

## Launcher Pages

Since SMBX2 b3, custom launcher pages for your episodes
have been possible to build. In the new release, however, these have
been greatly expanded.

![](images/image17.png)

Launcher pages generally consist of a .html file (and
optionally a .css file to go with it), an icon, and a file named
“info.json”. This documentation will mostly focus on info.json, but
will touch on a few other files.

The info.json file is the core of the launcher page. It
defines a lot of information about your episode. Here is a simple
example of an info.json file:

```json
{
    "mainPage": "index.html",
    "Title": "My Super Cool Episode",
    "allowPlayerSelection": false,
    "allowTwoPlayer": false,
    "episodeIcon": "icon.png",
    "progressDisplay": "percent"
}
```

This sets the episode name, determines the .html file
to use, disables selecting a character from the launcher (as well as
disabling 2 player mode), sets the episode icon, and sets the progress
to display as a percentage.

![](images/image33.png)

Here is a list of some useful fields for the info.json file:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>episodeIcon</td>
    <td>defines the file to use for the icon (should be 64x64 for best results)</td>
  </tr>
  <tr>
    <td>title</td>
    <td>defines the name of the episode</td>
  </tr>
  <tr>
    <td>allowedCharacters</td>
    <td>a list of character IDs to allow from the launcher (only supports mario, luigi, toad, peach, and link), e.g. [1,2] for only mario and luigi</td>
  </tr>
  <tr>
    <td>characterNames</td>
    <td>a list of names to use for the character selection, e.g. [“Demo”, “Iris”, “Kood”, “Raocow”, “Sheath”]</td>
  </tr>
  <tr>
    <td>current-version</td>
    <td>a list of version numbers, ranging from major to minor, e.g. [1, 0, 0]</td>
  </tr>
  <tr>
    <td>allowTwoPlayer</td>
    <td>set to false to disable character selection for player 2</td>
  </tr>
  <tr>
    <td>allowPlayerSelection</td>
    <td>set to false to disable character selection entirely</td>
  </tr>
  <tr>
    <td>allowSaveSelection</td>
    <td>set to false to disable the ability to choose a save slot (shouldn’t be commonly used)</td>
  </tr>
  <tr>
    <td>starIcon</td>
    <td>set to a 16x16 image file to use it in place of the stars icon on the launcher</td>
  </tr>
  <tr>
    <td>collectibles</td>
    <td>set to a string that determines the name of the collectible for this episode</td>
  </tr>
  <tr>
    <td>collectible</td>
    <td>an optional string to allow for a singular variant of the name of the episode collectible</td>
  </tr>
  <tr>
    <td>maxProgress</td>
    <td>if set to a number, this will be used instead of the total star count for measuring progress. Progress will be measured against Progress.progress set from Lua</td>
  </tr>
  <tr>
    <td>progressDisplay</td>
    <td>set to “percent” to display percentage progress for this episode (rather than using x/y format)</td>
  </tr>
  <tr>
    <td>customProgress</td>
    <td>if set to true without maxProgress being set, will force the usage of Progress.progress for measurement, displaying the raw value on the launcher</td>
  </tr>
  <tr>
    <td>noAchievementBorders</td>
    <td>set to true to disable the borders around achievement icons for this episode</td>
  </tr>
</tbody></table>

When constructing your launcher .html page, you can
create HTML elements with specific classes that will be automatically
populated. For example:

```html
<div class="_episodeTitle"></div>
```

This will automatically create an element that displays
your episode title. Here is a list of these automatic elements:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>_episodeTitle</td>
    <td>displays the name of the episode</td>
  </tr>
  <tr>
    <td>_episodeIcon</td>
    <td>displays the episode icon</td>
  </tr>
  <tr>
    <td>_stars</td>
    <td>displays the star icon or name, and the number of stars in the episode (will not display if no stars were found</td>
  </tr>
  <tr>
    <td>_starsIcon</td>
    <td>displays the star icon</td>
  </tr>
  <tr>
    <td>_starsCount</td>
    <td>displays the number of stars in the episode (will display 0 if no stars were found</td>
  </tr>
  <tr>
    <td>_starsContainer</td>
    <td>will not display if no stars were found in the episode</td>
  </tr>
  <tr>
    <td>_credits</td>
    <td>will display credits from the .wld file (will not display if no credits were found</td>
  </tr>
  <tr>
    <td>_creditsContainer</td>
    <td>will not display if no credits were found in the .wld file</td>
  </tr>
</tbody></table>

## Achievements

Achievements are a new feature that has been added to
the game\! These one-time collectables will appear in the launcher once
you’ve collected them, and you can design your own achievements for your
episodes.

To create an achievement, you must create a folder
named “achievements” in your episode folder.

![](images/image28.png)

Inside that folder, you can then create “ach-n.ini”
files, to define new achievements:

![](images/image24.png)

These .ini files are structured as information,
followed by lists of conditions. Here is an example .ini file:

```ini
name="My Super Rad Achievement"
desc="Do the thing!"
condition-1=true
condition-1-desc="Touch fuzzy."
condition-2=true
condition-2-desc="Get dizzy."
```

This section shows how to set up their data:

<table>
  <thead><tr>
    <th>Field</th>
    <th>Description</th>
  </tr></thead>
  <tbody><tr>
    <td>name</td>
    <td>Defines the name of the achievement</td>
  </tr>
  <tr>
    <td>desc</td>
    <td>Defines the description of the achievement</td>
  </tr>
  <tr>
    <td>collected-desc</td>
    <td>Rarely used, but defines a separate description to display when the achievement is unlocked.
	<br />If set to true, the achievement information will not be displayed until it is
   unlocked</td>
  </tr>
  <tr>
    <td>condition-#</td>
    <td>A numbered condition (e.g. condition-1). accepts a range of different values:<table><tbody>
		<tr>
			<td>true</td>
			<td>The condition will be considered cleared when it is progressed - must be done from Lua</td>
		</tr>
		<tr>
			<td>"myString"</td>
			<td>The condition will be considered cleared when it is progressed - progress will occur when the SMBX event with the given name is triggered</td>
		</tr>
		<tr>
			<td>#</td>
			<td>Any number - the condition will need to be triggered this number of times before it will be considered cleared - must be done from Lua</td>
		</tr>
		</tbody></table>
	</td>
  </tr>
  <tr>
    <td>condition-#-desc</td>
    <td>A description of the numbered condition. this must be provided for the condition to appear in the launcher</td>
  </tr>
</tbody></table>


As well as ach-n.ini, you can also provide an
ach-n.png. This should be a 64x64 icon for the achievement. You can also
opt to provide an ach-nl.png file (e.g. ach-1l.png), which will be used
for the icon when the achievement is still locked. Achievements will
usually require some Lua to set up, so please see below for more
details.

## Level Settings

In SMBX2 b4, you’ll have access to some new in-editor settings.
The first set of these are for the entire level. To find these new
settings, select Level from the
toolbar, and navigate down to
Properties.

![](images/image32.png)

In the window that appears, you will see the following
new settings:

![](images/image34.png)

### Mario Challenge

The first of these new settings is the “Appear in Mario
Challenge” setting. Unchecking this box will ensure your level will
never appear in the Mario Challenge level roulette. This is useful if
your level is a hub, a small bonus level, or might be impossible to
complete when thrown in from a Mario Challenge.

### Level Timer

The second set of these are the Level Timer settings.
This allows you to create a time limit for your levels. Simply check the
box, and set how many seconds the timer should last before it runs out
(and kills the player\!)

## Section Settings

SMBX2 b4 also adds some new functionality to level
sections, which allows you to do a lot more without touching Lua. The
first new feature available is vertical wrap. Checking this in the
section settings will allow players and NPCs to fall off the bottom of
the section and reappear at the top (or vice versa).

![](images/image12.png)

The rest of the settings require a bit more
explanation, and most can be adjusted further with Lua.

### Darkness

Sections now have settings to allow you to make them
dark. Simply checking the “Dark” checkbox in these settings will enable
this feature, and relevant blocks, BGOs and NPCs will automatically emit
light. However, there are a few extra options you can tweak, as shown
here:

![](images/image22.png)

<table>
<tr><td>Ambient Light</td>
<td>This determines the color of “darkness”. White means the darkness will not be visible at all, while black means nothing will be visible unless lit up.</td>
</tr>
<tr class="even">
<td>Shadow Type</td>
<td>You can choose what kind of shadows lights should cast in this section, if any. You can choose between no shadows, soft shadows, or hard shadows.</td>
</tr>
<tr class="odd">
<td>Falloff Type</td>
<td>This determines how light fades into the dark as it leaves the light source. By default this uses a realistic Inverse Square gradient, but there are a number of other options. Feel free to experiment.</td>
</tr>
<tr class="even">
<td>Maximum Lights</td>
<td>This sets the maximum number of light sources that can be visible at any one time. You should try to keep this low if you can.</td>
</tr>
<tr class="odd">
<td>Player Light &gt; Enabled</td>
<td>This enables a light source around the player.</td>
</tr>
<tr class="even">
<td>Player Light &gt; Color</td>
<td>Sets the color of the light source around the player. Black means no light at all.</td>
</tr>
<tr class="odd">
<td>Player Light &gt; Radius</td>
<td>Sets the radius of the light source, in pixels. A radius of 400 will stretch the light across the entire screen, for example.</td>
</tr>
<tr class="even">
<td>Player Light &gt; Brightness</td>
<td>Sets the brightness of the light source. 0 means no light at all, 1 means a normal brightness, and more than 1 means a very bright light.</td>
</tr>
<tr class="odd">
<td>Player Light &gt; Flicker</td>
<td>If this is checked, the light source around the player will flicker slightly.</td></tr>
</table>

### Effects

Sections can now have rendering effects applied to
them. These come in two flavors: weather and screen effects. These can
be combined for interesting combinations.

![](images/image2.png)

The options for Weather are:

1.  Rain - A moderate rainfall
    effect.
2.  Snow - A moderate snowfall
    effect.
3.  Fog - Fog that covers
    parts of the stage, obscuring the view.
4.  Sandstorm - Dust and sand
    particles blow across the stage from right to left.
5.  Cinders - Small sparks and
    flame particles drift up from below.
6.  Wisps - Etherial lights
    and will-o-wisps drist in the air.

The options for Screen Effects are:

1.  Wavy - Wobbles the screen
    slightly, as if in a heat haze or looking through water.
2.  Lava - Red light, sparks,
    and smoke drift up from the bottom of the section.
3.  Caustics - Effects similar
    to water reflecting light appear on blocks.
4.  Underwater - A combination of
    Wavy and
    Caustics, helpful
    for underwater sections.
5.  Mist - White mist drifts
    up from the bottom of the section.
6.  Sepia - Converts the
    screen to sepia tone.
7.  Grayscale - Converts the
    screen to black and white (grayscale).
8.  Inverted - Inverts the
    screen (light colors become dark, and vice versa).
9.  Gameboy - Colors are
    limited to the gameboy palette and screen resolution reduced.
10. Dithered Gameboy - Colors
    are limited to the gameboy palette and screen resolution reduced.
    Uses dithering to capture more detail.

These effects can also be accessed through Lua. See
[Added functionality to basegame
classes](lunalua#added-functionality-to-basegame-classes) below for more
information.

### Beat Timer

SMBX2 b4 adds Blinking Blocks and Timed Spike Blocks. These both
use a global timer call the “beat” to synchronize their behaviour. The
beat can be adjusted in the section settings, or manually through Lua
(See [Added functionality to basegame
classes](lunalua#added-functionality-to-basegame-classes) below for more
information)..

![](images/image27.png)

If the beat timer is enabled, then the section settings
will be used any time this section is entered, otherwise they will be
ignored.

<table><tbody>
<tr><td>BPM</td>
<td>This determines the speed of the beat timer. This is measured in Beats Per Minute, meaning the higher this number, the faster the beat. A BPM of 60 means one beat every second.</td>
</tr>
<tr class="even">
<td>Use Music Clock</td>
<td>Usually, you’ll want to sync the beat to the music. If you are doing that, you should check this box, as it ensures the beat stays in sync even if the game lags.</td>
</tr>
<tr class="odd">
<td>Time Signature</td>
<td>This determines how many beats are in a bar. The blocks using this timer in basegame all change state after one bar, so increasing this means increasing the time between state changes. Try to match this with the time signature of your music.</td></tr>
</tbody></table>
