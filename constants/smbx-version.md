# SMBX Version

The SMBX version constants help in making version-specific code. They represent a numeric value, meaning that comparison operators will work for running code on, for example, "all versions older than X".

```lua
if SMBX_VERSION <= VER_BETA4 then
    -- Execute old code.
end
```

| Constant | Description |
| --- | --- |
| SMBX_VERSION | Represents the current version. |
| VER_BETA4_PATCH_3_1 | SMBX2b4p3.1 |
| VER_BETA4_PATCH_3 | SMBX2b4p3 |
| VER_BETA4_PATCH_2_1 | SMBX2b4p2.1 |
| VER_BETA4_PATCH_2 | SMBX2b4p2 |
| VER_BETA4_HOTFIX | SMBX2b4p1 |
| VER_BETA4 | SMBX2b4 |
| VER_PAL_HOTFIX | SMBX2pal.1 |
| VER_PAL | SMBX2pal |
| VER_MAGLX3 | SMBX2mx3 |
| VER_BETA3 | SMBX2b3 |
| VER_BETA2 | SMBX2b2 |
| VER_BETA1 | SMBX2b1 |
